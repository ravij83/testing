<?php

// If necessary, modify the path in the require statement below to refer to the 
// location of your Composer autoload.php file.
require 'vendor/autoload.php';

use Aws\Pinpoint\PinpointClient;
use Aws\Exception\AwsException;

// Create an SesClient. Change the value of the region parameter if you're 
// using an AWS Region other than US West (Oregon). Change the value of the
// profile parameter if you want to use a profile in your credentials file
// other than the default.


$client = new PinpointClient([
    'version' => 'latest',
    'region'  => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIATTIMTMZPMN45QE7X',
        'secret' => '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',
    ]
]);


# The "From" address. This address has to be verified in Amazon Pinpoint in the region you're using to send email.
$sender_id = "MySenderID";

# The addresses on the "To" line. If your Amazon Pinpoint account is in the sandbox, these addresses also have to be verified.
$to_address = "+918072024258";

try {

    $result = $client->sendMessages([
      'ApplicationId' => 'f54ea37e7ec6408bbd26f3664138fb48',
      'MessageRequest' => [ // REQUIRED
        'Addresses' => [
          $to_address => [
            'ChannelType' => 'SMS',
          ],
        ],
        'MessageConfiguration' => [ // REQUIRED
          'SMSMessage' => [
                'Body' => 'Hi Dude, Test from AWS Pinpoint SMS API',
                'Keyword' => 'myKeyword',
                'MessageType' => 'TRANSACTIONAL',
                'OriginationNumber' => '',
                'SenderId' => $sender_id,
          ],
        ],
      ]
    ]);

    print $result;

    } catch (AwsException $e){

        // output error message if fails
        error_log($e->getMessage());
    }
?>